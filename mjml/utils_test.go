package mjml

import "testing"

var templateTest = TemplateNewUser{
	Name:     "Wahyudi",
	Email:    "wahyudi1@mail.com",
	Password: "1234",
}

var templateName = "TemplateNewUser"

func TestToMap(t *testing.T) {
	res, err := ToMap(templateTest)

	t.Logf("map result : %+v", res)

	if err != nil {
		t.Errorf("ERROR converting to map: %v", err)
	}
}

func TestGetType(t *testing.T) {
	res := GetType(templateTest)
	t.Logf("struct name : %s", res)

	if res != templateName {
		t.Errorf("ERROR get struct name: %s", res)
	}
}
