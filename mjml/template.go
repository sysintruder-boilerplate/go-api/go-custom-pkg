package mjml

type (
	TemplateNewUser struct {
		Name     string `mjml:"user"`
		Email    string `mjml:"email"`
		Password string `mjml:"password"`
	}
)
