package mjml

import "testing"

func TestLoadTemplate(t *testing.T) {
	template, err := loadTemplate(templateTest)

	t.Logf("loading template : %s", template)

	if err != nil {
		t.Errorf("ERROR loading template: %s", err.Message)
	}
}

func TestRender(t *testing.T) {
	template, err := Render(templateTest)

	t.Logf("rendering template : %s", template)

	if err != nil {
		t.Errorf("ERROR rendering template: %s", err.Message)
	}
}
