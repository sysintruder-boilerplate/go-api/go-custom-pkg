package mjml

import (
	"context"
	sysErr "errors"
	"fmt"
	errors "go-custom-pkg/helpers/errors"
	"os"
	"strings"

	"github.com/Boostport/mjml-go"
	"github.com/hoisie/mustache"
)

func Render(templateStruct interface{}) (string, *errors.Error) {
	template, err := loadTemplate(templateStruct)
	if err != nil {
		return "", err
	}

	paramsMap, mapErr := ToMap(templateStruct)
	if mapErr != nil {
		errMsg := mapErr.Error()
		errRes := *errors.InternalServerError
		errRes.Errors = append(errRes.Errors, errMsg)
		return "", &errRes
	}

	filled := mustache.Render(template, paramsMap)
	output, err := toHTML(filled)
	if err != nil {
		return "", err
	}

	return output, nil
}

func toHTML(template string) (string, *errors.Error) {
	var mjmlError mjml.Error
	output, err := mjml.ToHTML(context.Background(), template, mjml.WithMinify(true))
	if sysErr.As(err, &mjmlError) {
		errMsg := mjmlError.Message
		errRes := *errors.InternalServerError
		errRes.Errors = append(errRes.Errors, "error rendering mjml template", errMsg)
		return "", &errRes
	}

	return output, nil
}

func loadTemplate(templateStruct interface{}) (string, *errors.Error) {
	templateName := GetType(templateStruct)
	templateName = strings.Replace(templateName, "Template", "", 1)
	path := fmt.Sprintf("./internal/pkg/mjml/templates/%s.mjml", templateName)
	template, err := os.ReadFile(path)
	if err != nil {
		errMsg := err.Error()
		errRes := *errors.InternalServerError
		errRes.Errors = append(errRes.Errors, "error loading mjml template", errMsg)
		return "", &errRes
	}

	return string(template), nil
}
