package jwt

import (
	errors "go-custom-pkg/helpers/errors"
)

const RoleAdmin = "Admin"
const RoleAdminCode = "ADMIN"

const RoleUser = "User"
const RoleUserCode = "USER"

func AuthorizeAdmin(role string) *errors.Error {
	if role != RoleAdmin {
		errRes := *errors.UnauthorizedError
		errRes.AddError("can't access this feature")

		return &errRes
	}

	return nil
}
