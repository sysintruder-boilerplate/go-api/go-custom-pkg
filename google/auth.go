package google

import (
	errors "go-custom-pkg/helpers/errors"
	"go-custom-pkg/request"
)

type (
	TokenInfo struct {
		Issuer          string `json:"iss"`
		AuthorizedParty string `json:"azp"`
		Audience        string `json:"aud"`
		Subject         string `json:"sub"`
		Email           string `json:"email"`
		EmailVerified   string `json:"email_verified"`
		Nonce           string `json:"nonce"`
		IssuedAt        string `json:"iat"`
		Expires         string `json:"exp"`
		JWTID           string `json:"jti"`
		Algorithm       string `json:"alg"`
		KeyID           string `json:"kid"`
		Type            string `json:"typ"`
	}

	UserInfo struct {
		Email         string `json:"email"`
		EmailVerified bool   `json:"email_verified"`
		FamilyName    string `json:"family_name"`
		GivenName     string `json:"given_name"`
		Locale        string `json:"locale"`
		Name          string `json:"name"`
		Picture       string `json:"picture"`
		Subject       string `json:"sub"`
	}
)

const TOKEN_INFO_URL = "https://www.googleapis.com/oauth2/v3/tokeninfo"
const USER_INFO_URL = "https://www.googleapis.com/oauth2/v3/userinfo"

func VerifyIDToken(idToken string) (*TokenInfo, *errors.Error) {
	query := map[string]string{"id_token": idToken}
	tokenInfo, err := makeRequest[TokenInfo](TOKEN_INFO_URL, query)
	if err != nil {
		return nil, err
	}

	return tokenInfo, nil
}

func GetUserInfo(accessToken string) (*UserInfo, *errors.Error) {
	query := map[string]string{"access_token": accessToken}
	userInfo, err := makeRequest[UserInfo](USER_INFO_URL, query)
	if err != nil {
		return nil, err
	}

	return userInfo, nil
}

func makeRequest[T any](URL string, query map[string]string) (*T, *errors.Error) {
	var resp T
	err := request.GetRequest[T](URL, query, &resp)
	if err != nil {
		return nil, err
	}

	return &resp, nil
}
