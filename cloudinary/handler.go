package cloudinary

import (
	"context"
	errors "go-custom-pkg/helpers/errors"
	"net/http"

	"github.com/cloudinary/cloudinary-go/v2/api/uploader"
	"github.com/labstack/echo/v4"
)

func HandleUploadImageToCloudinary(c echo.Context, ctx context.Context, folder, form string) (*string, *errors.Error) {
	file, formErr := c.FormFile(form)
	if formErr != nil {
		if formErr == http.ErrMissingFile {
			return nil, nil
		}

		err := errors.InternalServerError
		err.AddError("error reading uploaded image")

		return nil, err
	}

	src, formErr := file.Open()
	if formErr != nil {
		err := errors.InternalServerError
		err.AddError("error opening uploaded image")

		return nil, err
	}

	defer src.Close()

	url, err := UploadSingle(ctx, src, uploader.UploadParams{Folder: folder})
	if err != nil {
		return nil, err
	}

	return url, err
}
