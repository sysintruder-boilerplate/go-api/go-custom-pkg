package cloudinary

import (
	"context"
	errors "go-custom-pkg/helpers/errors"

	"github.com/cloudinary/cloudinary-go/v2"
	"github.com/cloudinary/cloudinary-go/v2/api/uploader"
)

var cloudinaryClient *cloudinary.Cloudinary

func InitCloudinary() {
	name := "config.GlobalEnv.Cloudinary.Name"
	key := "config.GlobalEnv.Cloudinary.Key"
	secret := "config.GlobalEnv.Cloudinary.Secret"
	cloudinaryClient, _ = cloudinary.NewFromParams(name, key, secret)
}

func UploadSingle(ctx context.Context, file interface{}, params uploader.UploadParams) (*string, *errors.Error) {
	resp, err := cloudinaryClient.Upload.Upload(ctx, file, params)
	if err != nil {
		errMsg := err.Error()
		errRes := *errors.InternalServerError
		errRes.Errors = append(errRes.Errors, errMsg)
		return nil, &errRes
	}

	return &resp.SecureURL, nil
}
