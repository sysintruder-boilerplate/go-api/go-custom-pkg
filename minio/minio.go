package minio

import (
	"context"
	errors "go-custom-pkg/helpers/errors"
	"io"

	"github.com/minio/minio-go/v7"
	"github.com/minio/minio-go/v7/pkg/credentials"
)

var minioClient *minio.Client

func InitMinio() {
	url := "config.GlobalEnv.Minio.URL"
	access := "config.GlobalEnv.Minio.Access"
	secret := "config.GlobalEnv.Minio.Secret"
	useSSL := true
	minioClient, _ = minio.New(url, &minio.Options{
		Creds:  credentials.NewStaticV4(access, secret, ""),
		Secure: useSSL,
	})
}

func UploadObject(ctx context.Context, file io.Reader, fileName string, filesize int, opts minio.PutObjectOptions) (*string, *errors.Error) {
	url := "config.GlobalEnv.Minio.URL"
	bucket := "config.GlobalEnv.Minio.Bucket"
	_, err := minioClient.PutObject(ctx, bucket, fileName, file, int64(filesize), opts)
	if err != nil {
		errMsg := err.Error()
		errRes := *errors.InternalServerError
		errRes.Errors = append(errRes.Errors, errMsg)
		return nil, &errRes
	}
	fileUrl := "https://" + url + "/" + bucket + "/" + fileName

	return &fileUrl, nil
}
