package minio

import (
	"context"
	errors "go-custom-pkg/helpers/errors"
	"net/http"

	"github.com/labstack/echo/v4"
	"github.com/minio/minio-go/v7"
)

func HandleUploadImageToMinio(c echo.Context, ctx context.Context, form string) (*string, *errors.Error) {
	file, formErr := c.FormFile(form)
	if formErr != nil {
		if formErr == http.ErrMissingFile {
			return nil, nil
		}

		err := errors.InternalServerError
		err.AddError("error checking uploaded image")

		return nil, err
	}

	src, formErr := file.Open()
	if formErr != nil {
		err := errors.InternalServerError
		err.AddError("error opening uploaded image")

		return nil, err
	}

	defer src.Close()

	contentType := file.Header["Content-Type"][0]
	filename := file.Filename
	filesize := file.Size
	obj, err := UploadObject(ctx, src, filename, int(filesize), minio.PutObjectOptions{ContentType: contentType})
	if err != nil {
		return nil, err
	}

	return obj, nil
}
