package sqlite

import (
	"fmt"

	"github.com/jmoiron/sqlx"
	_ "github.com/mattn/go-sqlite3"
)

var Db *sqlx.DB
var dbDriver = "sqlite3"

func InitSqlite() {
	dsn := "config.GlobalEnv.SQLite.DataSourceName"
	Db = sqlx.MustConnect(dbDriver, dsn)

	fmt.Print(":: SQLite connected\n")
}
