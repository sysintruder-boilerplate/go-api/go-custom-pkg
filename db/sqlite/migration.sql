-- SQLite
CREATE TABLE IF NOT EXISTS users (
    id INTEGER PRIMARY KEY,
    sub TEXT NOT NULL,
    name TEXT NOT NULL,
    email TEXT NOT NULL,
    picture TEXT NULL
)
