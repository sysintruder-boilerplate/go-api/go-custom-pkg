package db_mysql

import (
	"fmt"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
)

var Db *sqlx.DB
var dbDriver = "mysql"

func InitMysql() {
	user := "config.GlobalEnv.MySQL.User"
	pass := "config.GlobalEnv.MySQL.Pass"
	host := "config.GlobalEnv.MySQL.Host"
	port := "config.GlobalEnv.MySQL.Port"
	schema := "config.GlobalEnv.MySQL.Schema"
	dsn := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s", user, pass, host, port, schema)
	Db = sqlx.MustConnect(dbDriver, dsn)

	fmt.Print(":: MySQL connected\n")
}
