package activitylog

import (
	"encoding/json"
	"fmt"
	"mime/multipart"
	"net/http"
	"net/url"

	"github.com/labstack/echo/v4"
)

type (
	Log struct {
		Menu        string
		Action      string
		Description string
	}
	Req struct {
		Method           string
		URL              *url.URL
		ContentLength    int64
		TransferEncoding []string
		Close            bool
		Host             string
		Form             url.Values
		MultipartForm    *multipart.Form
		Trailer          http.Header
		RemoteAddr       string
		RequestURI       string
	}
)

func LogActivitySaver(c echo.Context, resBody []byte) {
	rawReq := c.Request()
	if rawReq.Method == "GET" {
		return
	}

	rawHeader := rawReq.Header
	logReq := Req{
		Method:           rawReq.Method,
		URL:              rawReq.URL,
		ContentLength:    rawReq.ContentLength,
		TransferEncoding: rawReq.TransferEncoding,
		Close:            rawReq.Close,
		Host:             rawReq.Host,
		Form:             rawReq.Form,
		MultipartForm:    rawReq.MultipartForm,
		Trailer:          rawReq.Trailer,
		RemoteAddr:       rawReq.RemoteAddr,
		RequestURI:       rawReq.RequestURI,
	}
	req, _ := json.Marshal(logReq)
	header, _ := json.Marshal(rawHeader)

	fmt.Printf("header:%#v\nreq:%#v\nres:%#v\n", string(header), string(req), string(resBody))
	// admin := jwt.Authorize(c)
	// adminID := 0
	// if admin != nil {
	// 	adminID = admin.ID
	// }

	// logAct := adapter.LogActivity{
	// 	UserID:      adminID,
	// 	Menu:        rawReq.RequestURI,
	// 	Action:      rawReq.Method,
	// 	APIHeader:   string(header),
	// 	APIRequest:  string(req),
	// 	APIResponse: string(resBody),
	// }
	//
	// timeout := 15 * time.Second
	// ctx, cancel := context.WithTimeout(context.Background(), timeout)
	//
	// defer cancel()
	//
	// repo := ActivityRepo{
	// 	ctx: ctx,
	// 	db:  mysql.Db,
	// }
	//
	// _, err := repo.AddLogActivity(logAct)
	// if err != nil {
	// 	log.Print(err)
	// }
}

// USAGE AS ECHO MIDDLEWARE
// e.Use(middleware.BodyDump(func(c echo.Context, _, resBody []byte) {
// 	activitylog.LogActivitySaver(c, resBody)
// }))
