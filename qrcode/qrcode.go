package qrcode

import (
	"encoding/base64"

	qrcode "github.com/skip2/go-qrcode"
)

func GenerateQRCodeforHTML(data string) string {
	raw := GenerateQRCode(data, 256)
	image := qrCodeToBase64HTML(raw)

	return image
}
func GenerateQRCode(data string, width int) []byte {
	var png []byte
	png, _ = qrcode.Encode(data, qrcode.Medium, width)

	return png
}
func qrCodeToBase64HTML(qrcode []byte) string {
	var HEADER = "data:image/png;base64,"
	image := base64.StdEncoding.EncodeToString(qrcode)

	return HEADER + image
}
