package cron

import (
	"fmt"
	"time"

	"github.com/robfig/cron/v3"
)

func InitDailyCron() {
	jakartaTime, _ := time.LoadLocation("Asia/Jakarta")
	scheduler := cron.New(cron.WithLocation(jakartaTime))

	defer scheduler.Stop()

	scheduler.AddFunc("0 0 * * *", func() {
		fmt.Print(":: Daily cron is running")
	})

	go scheduler.Start()
}

func InitHourlyCron() {
	jakartaTime, _ := time.LoadLocation("Asia/Jakarta")
	scheduler := cron.New(cron.WithLocation(jakartaTime))

	defer scheduler.Stop()

	scheduler.AddFunc("0 * * * *", func() {
		fmt.Print(":: Hourly cron is running")
	})

	go scheduler.Start()
}
