package mailjet

import (
	errors "go-custom-pkg/helpers/errors"
	"go-custom-pkg/mjml"

	"github.com/mailjet/mailjet-apiv3-go/v4"
)

type Recipient struct {
	Name  string
	Email string
}

type SendWithTemplateReq struct {
	Recipient Recipient
	Subject   string
	Template  interface{}
}

var mailjetClient *mailjet.Client
var email string

func InitMailjet() {
	email = "config.GlobalEnv.Mailjet.Email"
	privateKey := "config.GlobalEnv.Mailjet.PrivateKey"
	publicKey := "config.GlobalEnv.Mailjet.PublicKey"
	mailjetClient = mailjet.NewMailjetClient(publicKey, privateKey)
}

func SendWithTemplate(mail SendWithTemplateReq) (*mailjet.ResultsV31, *errors.Error) {
	html, err := mjml.Render(mail.Template)
	if err != nil {
		return nil, err
	}

	messagesInfo := []mailjet.InfoMessagesV31{
		{
			From: &mailjet.RecipientV31{
				Email: email,
				Name:  email,
			},
			To: &mailjet.RecipientsV31{
				mailjet.RecipientV31{
					Email: mail.Recipient.Email,
					Name:  mail.Recipient.Name,
				},
			},
			Subject:  mail.Subject,
			HTMLPart: html,
		},
	}
	messages := mailjet.MessagesV31{Info: messagesInfo}
	resp, sendErr := mailjetClient.SendMailV31(&messages)
	if sendErr != nil {
		errMsg := sendErr.Error()
		errRes := *errors.InternalServerError
		errRes.Errors = append(errRes.Errors, errMsg)
		return nil, &errRes
	}

	return resp, nil
}
