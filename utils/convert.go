package utils

func NumberToAlphabet(n int) string {
	result := ""

	for n > 0 {
		mod := (n - 1) % 26
		result = string(rune('A'+mod)) + result
		n = (n - mod) / 26
	}

	return result
}

func IntToCharStr(i int) string {
	return string(rune('A' - 1 + i))
}
