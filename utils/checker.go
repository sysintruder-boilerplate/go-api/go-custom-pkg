package utils

import "reflect"

func IsArrayByte(v interface{}) bool {
	t := reflect.TypeOf(v)

	return t.Kind() == reflect.Slice && t.Elem().Kind() == reflect.Uint8
}
