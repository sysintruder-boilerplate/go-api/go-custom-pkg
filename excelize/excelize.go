package excelize

import (
	"fmt"
	"reflect"

	errors "go-custom-pkg/helpers/errors"
	"go-custom-pkg/utils"

	"github.com/xuri/excelize/v2"
)

type (
	setCellAsImageArgs struct {
		file      *excelize.File
		image     []byte
		altImage  string
		sheet     string
		cell      string
		col       string
		colWidth  float64
		row       int
		rowHeight float64
	}
)

func GenerateExcel[T any](sheetName string, data []T) ([]byte, *errors.Error) {
	if sheetName == "" {
		sheetName = "Sheet One"
	}

	excelFile := excelize.NewFile()
	excelFile.SetSheetName(excelFile.GetSheetName(0), sheetName)
	var typeSample T
	value := reflect.ValueOf(typeSample)
	numFields := value.NumField()
	valueType := value.Type()

	for i := 0; i < numFields; i++ {
		field := valueType.Field(i).Tag.Get("xlsx")
		if field == "" {
			field = utils.SplitCamelCase(valueType.Field(i).Name)
		}

		character := utils.NumberToAlphabet(i + 1)
		cell := fmt.Sprintf("%s%d", character, 1)
		excelFile.SetCellValue(sheetName, cell, field)
	}

	for i, item := range data {
		v := reflect.ValueOf(item)

		for j := 0; j < numFields; j++ {
			col := utils.NumberToAlphabet(j + 1)
			cell := fmt.Sprintf("%s%d", col, i+2)
			val := v.Field(j).Interface()

			if utils.IsArrayByte(val) {
				setCellAsImage(setCellAsImageArgs{
					file:      excelFile,
					image:     val.([]byte),
					altImage:  "QR Code",
					sheet:     sheetName,
					cell:      cell,
					col:       col,
					colWidth:  20,
					row:       i + 2,
					rowHeight: 64,
				})
			} else {
				excelFile.SetCellValue(sheetName, cell, val)
			}
		}
	}

	buffer, buffErr := excelFile.WriteToBuffer()
	if buffErr != nil {
		err := *errors.InternalServerError
		err.AddError(buffErr.Error())

		return nil, &err
	}

	return buffer.Bytes(), nil
}

func setCellAsImage(args setCellAsImageArgs) {
	args.file.SetColWidth(args.sheet, args.col, args.col, args.colWidth)
	args.file.SetRowHeight(args.sheet, args.row, args.rowHeight)
	args.file.AddPictureFromBytes(args.sheet, args.cell, &excelize.Picture{
		Extension: ".png",
		File:      args.image,
		Format:    &excelize.GraphicOptions{AltText: args.altImage},
	})
}
